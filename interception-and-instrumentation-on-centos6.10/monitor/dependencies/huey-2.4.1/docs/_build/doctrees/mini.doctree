���f      �docutils.nodes��document���)��}�(�	rawsource�� ��children�]�(h �target���)��}�(h�	.. _mini:�h]��
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]��refid��mini�u�tagname�h	�line�K�parent�hhh�source��%/home/charles/code/huey/docs/mini.rst�ubh �section���)��}�(hhh]�(h �title���)��}�(h�	Mini-Huey�h]�h �Text����	Mini-Huey�����}�(hh+hh)hhh NhNubah}�(h]�h]�h]�h]�h]�uhh'hh$hhh h!hKubh �	paragraph���)��}�(hX�  :py:class:`MiniHuey` provides a very lightweight huey-like API that may be
useful for certain applications. The ``MiniHuey`` consumer runs inside a
greenlet in your main application process.  This means there is no separate
consumer process to manage, nor is there any persistence for the
enqueued/scheduled tasks; whenever a task is enqueued or is scheduled to run, a
new greenlet is spawned to execute the task.�h]�(�sphinx.addnodes��pending_xref���)��}�(h�:py:class:`MiniHuey`�h]�h �literal���)��}�(hhDh]�h.�MiniHuey�����}�(hhhhHubah}�(h]�h]�(�xref��py��py-class�eh]�h]�h]�uhhFhhBubah}�(h]�h]�h]�h]�h]��reftype��class��	refdomain�hS�refexplicit���	py:module�N�py:class�N�	reftarget��MiniHuey��refdoc��mini��refwarn��uhh@h h!hKhh;ubh.�\ provides a very lightweight huey-like API that may be
useful for certain applications. The �����}�(h�\ provides a very lightweight huey-like API that may be
useful for certain applications. The �hh;hhh NhNubhG)��}�(h�``MiniHuey``�h]�h.�MiniHuey�����}�(hhhhnubah}�(h]�h]�h]�h]�h]�uhhFhh;ubh.X!   consumer runs inside a
greenlet in your main application process.  This means there is no separate
consumer process to manage, nor is there any persistence for the
enqueued/scheduled tasks; whenever a task is enqueued or is scheduled to run, a
new greenlet is spawned to execute the task.�����}�(hX!   consumer runs inside a
greenlet in your main application process.  This means there is no separate
consumer process to manage, nor is there any persistence for the
enqueued/scheduled tasks; whenever a task is enqueued or is scheduled to run, a
new greenlet is spawned to execute the task.�hh;hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKhh$hhubh:)��}�(h�*MiniHuey* may be useful if:�h]�(h �emphasis���)��}�(h�
*MiniHuey*�h]�h.�MiniHuey�����}�(hhhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�ubh.� may be useful if:�����}�(h� may be useful if:�hh�hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKhh$hhubh �bullet_list���)��}�(hhh]�(h �	list_item���)��}�(h�'Your application is a WSGI application.�h]�h:)��}�(hh�h]�h.�'Your application is a WSGI application.�����}�(hh�hh�ubah}�(h]�h]�h]�h]�h]�uhh9h h!hKhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhh h!hNubh�)��}�(h�nYour tasks do stuff like check for spam, send email, make requests to
web-based APIs, query a database server.�h]�h:)��}�(h�nYour tasks do stuff like check for spam, send email, make requests to
web-based APIs, query a database server.�h]�h.�nYour tasks do stuff like check for spam, send email, make requests to
web-based APIs, query a database server.�����}�(hh�hh�ubah}�(h]�h]�h]�h]�h]�uhh9h h!hKhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhh h!hNubh�)��}�(h�_You do not need automatic retries, persistence for your message queue,
dynamic task revocation.�h]�h:)��}�(h�_You do not need automatic retries, persistence for your message queue,
dynamic task revocation.�h]�h.�_You do not need automatic retries, persistence for your message queue,
dynamic task revocation.�����}�(hh�hh�ubah}�(h]�h]�h]�h]�h]�uhh9h h!hKhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhh h!hNubh�)��}�(h�iYou wish to keep things nice and simple and don't want the overhead of
additional process(es) to manage.
�h]�h:)��}�(h�hYou wish to keep things nice and simple and don't want the overhead of
additional process(es) to manage.�h]�h.�jYou wish to keep things nice and simple and don’t want the overhead of
additional process(es) to manage.�����}�(hh�hh�ubah}�(h]�h]�h]�h]�h]�uhh9h h!hKhh�ubah}�(h]�h]�h]�h]�h]�uhh�hh�hhh h!hNubeh}�(h]�h]�h]�h]�h]��bullet��*�uhh�h h!hKhh$hhubh:)��}�(h�"*MiniHuey* may be a bad choice if:�h]�(h�)��}�(h�
*MiniHuey*�h]�h.�MiniHuey�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhh�hj  ubh.� may be a bad choice if:�����}�(h� may be a bad choice if:�hj  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKhh$hhubh�)��}�(hhh]�(h�)��}�(h�AYour application is incompatible with gevent (e.g. uses asyncio).�h]�h:)��}�(hj6  h]�h.�AYour application is incompatible with gevent (e.g. uses asyncio).�����}�(hj6  hj8  ubah}�(h]�h]�h]�h]�h]�uhh9h h!hKhj4  ubah}�(h]�h]�h]�h]�h]�uhh�hj1  hhh h!hNubh�)��}�(h��Your tasks do stuff like process large files, crunch numbers, parse large XML
or JSON documents, or other CPU or disk-intensive work.�h]�h:)��}�(h��Your tasks do stuff like process large files, crunch numbers, parse large XML
or JSON documents, or other CPU or disk-intensive work.�h]�h.��Your tasks do stuff like process large files, crunch numbers, parse large XML
or JSON documents, or other CPU or disk-intensive work.�����}�(hjQ  hjO  ubah}�(h]�h]�h]�h]�h]�uhh9h h!hKhjK  ubah}�(h]�h]�h]�h]�h]�uhh�hj1  hhh h!hNubh�)��}�(h��You need a persistent store for messages and results, so the consumer can be
restarted without losing any unprocessed messages.
�h]�h:)��}�(h�You need a persistent store for messages and results, so the consumer can be
restarted without losing any unprocessed messages.�h]�h.�You need a persistent store for messages and results, so the consumer can be
restarted without losing any unprocessed messages.�����}�(hji  hjg  ubah}�(h]�h]�h]�h]�h]�uhh9h h!hKhjc  ubah}�(h]�h]�h]�h]�h]�uhh�hj1  hhh h!hNubeh}�(h]�h]�h]�h]�h]�j  j  uhh�h h!hKhh$hhubh:)��}�(h�kIf you are not sure, then you should probably not use *MiniHuey*. Use the
regular :py:class:`Huey` instead.�h]�(h.�6If you are not sure, then you should probably not use �����}�(h�6If you are not sure, then you should probably not use �hj�  hhh NhNubh�)��}�(h�
*MiniHuey*�h]�h.�MiniHuey�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh.�. Use the
regular �����}�(h�. Use the
regular �hj�  hhh NhNubhA)��}�(h�:py:class:`Huey`�h]�hG)��}�(hj�  h]�h.�Huey�����}�(hhhj�  ubah}�(h]�h]�(hR�py��py-class�eh]�h]�h]�uhhFhj�  ubah}�(h]�h]�h]�h]�h]��reftype��class��	refdomain�j�  �refexplicit��hbNhcNhd�Huey�hfhghh�uhh@h h!hKhj�  ubh.�	 instead.�����}�(h�	 instead.�hj�  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKhh$hhubh:)��}�(h�Usage and task declaration:�h]�h.�Usage and task declaration:�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hK"hh$hhubh?�index���)��}�(hhh]�h}�(h]�h]�h]�h]�h]��entries�]�(�single��MiniHuey (built-in class)��MiniHuey�hNt�auhj�  hh$hhh NhNubh?�desc���)��}�(hhh]�(h?�desc_signature���)��}�(h�7MiniHuey([name='huey'[, interval=1[, pool_size=None]]])�h]�(h?�desc_annotation���)��}�(h�class �h]�h.�class �����}�(hhhj�  hhh NhNubah}�(h]�h]�h]�h]�h]��	xml:space��preserve�uhj�  hj�  hhh h!hKtubh?�	desc_name���)��}�(hj�  h]�h.�MiniHuey�����}�(hhhj  hhh NhNubah}�(h]�h]�h]�h]�h]�j   j  uhj  hj�  hhh h!hKtubh?�desc_parameterlist���)��}�(h�-[name='huey', [interval=1, [pool_size=None]]]�h]�h?�desc_optional���)��}�(h�-[name='huey', [interval=1, [pool_size=None]]]�h]�(h?�desc_parameter���)��}�(h�name='huey'�h]�h.�name='huey'�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�j   j  uhj  hj  ubj  )��}�(h�[interval=1, [pool_size=None]]�h]�(j  )��}�(h�
interval=1�h]�h.�
interval=1�����}�(hhhj1  ubah}�(h]�h]�h]�h]�h]�j   j  uhj  hj-  ubj  )��}�(h�[pool_size=None]�h]�j  )��}�(h�pool_size=None�h]�h.�pool_size=None�����}�(hhhjC  ubah}�(h]�h]�h]�h]�h]�j   j  uhj  hj?  ubah}�(h]�h]�h]�h]�h]�j   j  uhj  hj-  ubeh}�(h]�h]�h]�h]�h]�j   j  uhj  hj  ubeh}�(h]�h]�h]�h]�h]�j   j  uhj  hj  ubah}�(h]�h]�h]�h]�h]�j   j  uhj  hj�  hhh h!hKtubeh}�(h]�j�  ah]�h]�j�  ah]�h]��first���module�N�class�h�fullname�j�  uhj�  hj�  hhh h!hKtubh?�desc_content���)��}�(hhh]�(h �
field_list���)��}�(hhh]�h �field���)��}�(hhh]�(h �
field_name���)��}�(h�
Parameters�h]�h.�
Parameters�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj  h h!hK ubh �
field_body���)��}�(hhh]�h�)��}�(hhh]�(h�)��}�(hhh]�h:)��}�(h�/name (str) -- Name given to this huey instance.�h]�(h?�literal_strong���)��}�(h�name�h]�h.�name�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]��refspecific��uhj�  hj�  ubh.� (�����}�(hhhj�  ubhA)��}�(hhh]�h?�literal_emphasis���)��}�(h�str�h]�h.�str�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj�  ubah}�(h]�h]�h]�h]�h]��	refdomain��py��refexplicit���reftype�jq  �	reftarget�j�  j�  �uhh@hj�  ubh.�)�����}�(hhhj�  ubh.� – �����}�(hhhj�  ubh.�!Name given to this huey instance.�����}�(h�!Name given to this huey instance.�hj�  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9hj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh�)��}�(hhh]�h:)��}�(h�Hinterval (int) -- How frequently to check for scheduled tasks (seconds).�h]�(j�  )��}�(h�interval�h]�h.�interval�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�j�  �uhj�  hj�  ubh.� (�����}�(hhhj�  ubhA)��}�(hhh]�j�  )��}�(h�int�h]�h.�int�����}�(hhhj	  ubah}�(h]�h]�h]�h]�h]�uhj�  hj  ubah}�(h]�h]�h]�h]�h]��	refdomain�j�  �refexplicit���reftype�jq  �	reftarget�j  j�  �uhh@hj�  ubh.�)�����}�(hhhj�  ubh.� – �����}�(hhhj�  ubh.�6How frequently to check for scheduled tasks (seconds).�����}�(h�6How frequently to check for scheduled tasks (seconds).�hj�  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9hj�  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubh�)��}�(hhh]�h:)��}�(h�Bpool_size (int) -- Limit number of concurrent tasks to given size.�h]�(j�  )��}�(h�	pool_size�h]�h.�	pool_size�����}�(hhhjA  ubah}�(h]�h]�h]�h]�h]�j�  �uhj�  hj=  ubh.� (�����}�(hhhj=  ubhA)��}�(hhh]�j�  )��}�(h�int�h]�h.�int�����}�(hhhjV  ubah}�(h]�h]�h]�h]�h]�uhj�  hjS  ubah}�(h]�h]�h]�h]�h]��	refdomain�j�  �refexplicit���reftype�jq  �	reftarget�jX  j�  �uhh@hj=  ubh.�)�����}�(hhhj=  ubh.� – �����}�(hhhj=  ubh.�/Limit number of concurrent tasks to given size.�����}�(h�/Limit number of concurrent tasks to given size.�hj=  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9hj:  ubah}�(h]�h]�h]�h]�h]�uhh�hj�  ubeh}�(h]�h]�h]�h]�h]�uhh�hj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hj  ubeh}�(h]�h]�h]�h]�h]�uhj}  hjz  ubah}�(h]�h]�h]�h]�h]�uhjx  hju  hhh NhNubj�  )��}�(hhh]�h}�(h]�h]�h]�h]�h]��entries�]�(j�  �task() (MiniHuey method)��MiniHuey.task�hNt�auhj�  hju  hhh NhNubj�  )��}�(hhh]�(j�  )��}�(h�task([validate_func=None])�h]�(j  )��}�(h�task�h]�h.�task�����}�(hhhj�  hhh NhNubah}�(h]�h]�h]�h]�h]�j   j  uhj  hj�  hhh h!hKXubj  )��}�(h�[validate_func=None]�h]�j  )��}�(h�[validate_func=None]�h]�j  )��}�(h�validate_func=None�h]�h.�validate_func=None�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�j   j  uhj  hj�  ubah}�(h]�h]�h]�h]�h]�j   j  uhj  hj�  ubah}�(h]�h]�h]�h]�h]�j   j  uhj  hj�  hhh h!hKXubeh}�(h]�j�  ah]�h]�j�  ah]�h]�jo  �jp  Njq  j�  jr  j�  uhj�  hj�  hhh h!hKXubjt  )��}�(hhh]�(h:)��}�(h��Task decorator similar to :py:meth:`Huey.task` or :py:meth:`Huey.periodic_task`.
For tasks that should be scheduled automatically at regular intervals,
simply provide a suitable :py:func:`crontab` definition.�h]�(h.�Task decorator similar to �����}�(h�Task decorator similar to �hj�  hhh NhNubhA)��}�(h�:py:meth:`Huey.task`�h]�hG)��}�(hj�  h]�h.�Huey.task()�����}�(hhhj�  ubah}�(h]�h]�(hR�py��py-meth�eh]�h]�h]�uhhFhj�  ubah}�(h]�h]�h]�h]�h]��reftype��meth��	refdomain�j  �refexplicit��hbNhcj�  hd�	Huey.task�hfhghh�uhh@h h!hK,hj�  ubh.� or �����}�(h� or �hj�  hhh NhNubhA)��}�(h�:py:meth:`Huey.periodic_task`�h]�hG)��}�(hj  h]�h.�Huey.periodic_task()�����}�(hhhj  ubah}�(h]�h]�(hR�py��py-meth�eh]�h]�h]�uhhFhj  ubah}�(h]�h]�h]�h]�h]��reftype��meth��	refdomain�j'  �refexplicit��hbNhcj�  hd�Huey.periodic_task�hfhghh�uhh@h h!hK,hj�  ubh.�c.
For tasks that should be scheduled automatically at regular intervals,
simply provide a suitable �����}�(h�c.
For tasks that should be scheduled automatically at regular intervals,
simply provide a suitable �hj�  hhh NhNubhA)��}�(h�:py:func:`crontab`�h]�hG)��}�(hj>  h]�h.�	crontab()�����}�(hhhj@  ubah}�(h]�h]�(hR�py��py-func�eh]�h]�h]�uhhFhj<  ubah}�(h]�h]�h]�h]�h]��reftype��func��	refdomain�jJ  �refexplicit��hbNhcj�  hd�crontab�hfhghh�uhh@h h!hK,hj�  ubh.� definition.�����}�(h� definition.�hj�  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK,hj�  hhubh:)��}�(h�wThe decorated task will gain a ``schedule()`` method which can be used
like the :py:meth:`TaskWrapper.schedule` method.�h]�(h.�The decorated task will gain a �����}�(h�The decorated task will gain a �hje  hhh NhNubhG)��}�(h�``schedule()``�h]�h.�
schedule()�����}�(hhhjn  ubah}�(h]�h]�h]�h]�h]�uhhFhje  ubh.�# method which can be used
like the �����}�(h�# method which can be used
like the �hje  hhh NhNubhA)��}�(h�:py:meth:`TaskWrapper.schedule`�h]�hG)��}�(hj�  h]�h.�TaskWrapper.schedule()�����}�(hhhj�  ubah}�(h]�h]�(hR�py��py-meth�eh]�h]�h]�uhhFhj�  ubah}�(h]�h]�h]�h]�h]��reftype��meth��	refdomain�j�  �refexplicit��hbNhcj�  hd�TaskWrapper.schedule�hfhghh�uhh@h h!hK0hje  ubh.� method.�����}�(h� method.�hje  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK0hj�  hhubh:)��}�(h�Examples task declarations:�h]�h.�Examples task declarations:�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hK3hj�  hhubh �literal_block���)��}�(h��from huey import crontab
from huey.contrib.mini import MiniHuey

huey = MiniHuey()

@huey.task()
def fetch_url(url):
    return urlopen(url).read()

@huey.task(crontab(minute='0', hour='4'))
def run_backup():
    pass�h]�h.��from huey import crontab
from huey.contrib.mini import MiniHuey

huey = MiniHuey()

@huey.task()
def fetch_url(url):
    return urlopen(url).read()

@huey.task(crontab(minute='0', hour='4'))
def run_backup():
    pass�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�j   j  �language��python��linenos���highlight_args�}�uhj�  h h!hK5hj�  hhubh:)��}�(h�UExample usage. Running tasks and getting results work about the same as
regular Huey:�h]�h.�UExample usage. Running tasks and getting results work about the same as
regular Huey:�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hKDhj�  hhubj�  )��}�(h��# Executes the task asynchronously in a new greenlet.
result = fetch_url('https://google.com/')

# Wait for the task to finish.
html = result.get()�h]�h.��# Executes the task asynchronously in a new greenlet.
result = fetch_url('https://google.com/')

# Wait for the task to finish.
html = result.get()�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�j   j  j�  �python�j�  �j�  }�uhj�  h h!hKGhj�  hhubh:)��}�(h� Scheduling a task for execution:�h]�h.� Scheduling a task for execution:�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hKOhj�  hhubj�  )��}�(h��# Fetch in ~30s.
result = fetch_url.schedule(('https://google.com',), delay=30)

# Wait until result is ready, takes ~30s.
html = result.get()�h]�h.��# Fetch in ~30s.
result = fetch_url.schedule(('https://google.com',), delay=30)

# Wait until result is ready, takes ~30s.
html = result.get()�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�j   j  j�  �python�j�  �j�  }�uhj�  h h!hKQhj�  hhubeh}�(h]�h]�h]�h]�h]�uhjs  hj�  hhh h!hKXubeh}�(h]�h]�h]�h]�h]��domain��py��objtype��method��desctype�j  �noindex��uhj�  hhhju  h NhNubj�  )��}�(hhh]�h}�(h]�h]�h]�h]�h]��entries�]�(j�  �start() (MiniHuey method)��MiniHuey.start�hNt�auhj�  hju  hhh NhNubj�  )��}�(hhh]�(j�  )��}�(h�start()�h]�(j  )��}�(h�start�h]�h.�start�����}�(hhhj0  hhh NhNubah}�(h]�h]�h]�h]�h]�j   j  uhj  hj,  hhh h!hKpubj  )��}�(hhh]�h}�(h]�h]�h]�h]�h]�j   j  uhj  hj,  hhh h!hKpubeh}�(h]�j'  ah]�h]�j'  ah]�h]�jo  �jp  Njq  j�  jr  j'  uhj�  hj)  hhh h!hKpubjt  )��}�(hhh]�(h:)��}�(h��Start the scheduler in a new green thread. The scheduler is needed if
you plan to schedule tasks for execution using the ``schedule()``
method, or if you want to run periodic tasks.�h]�(h.�yStart the scheduler in a new green thread. The scheduler is needed if
you plan to schedule tasks for execution using the �����}�(h�yStart the scheduler in a new green thread. The scheduler is needed if
you plan to schedule tasks for execution using the �hjP  hhh NhNubhG)��}�(h�``schedule()``�h]�h.�
schedule()�����}�(hhhjY  ubah}�(h]�h]�h]�h]�h]�uhhFhjP  ubh.�.
method, or if you want to run periodic tasks.�����}�(h�.
method, or if you want to run periodic tasks.�hjP  hhh NhNubeh}�(h]�h]�h]�h]�h]�uhh9h h!hK[hjM  hhubh:)��}�(h��Typically this method should be called when your application starts up.
For example, a WSGI application might do something like:�h]�h.��Typically this method should be called when your application starts up.
For example, a WSGI application might do something like:�����}�(hjt  hjr  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hK_hjM  hhubj�  )��}�(hXo  # Always apply gevent monkey-patch before anything else!
from gevent import monkey; monkey.patch_all()

from my_app import app  # flask/bottle/whatever WSGI app.
from my_app import mini_huey

# Start the scheduler. Returns immediately.
mini_huey.start()

# Run the WSGI server.
from gevent.pywsgi import WSGIServer
WSGIServer(('127.0.0.1', 8000), app).serve_forever()�h]�h.Xo  # Always apply gevent monkey-patch before anything else!
from gevent import monkey; monkey.patch_all()

from my_app import app  # flask/bottle/whatever WSGI app.
from my_app import mini_huey

# Start the scheduler. Returns immediately.
mini_huey.start()

# Run the WSGI server.
from gevent.pywsgi import WSGIServer
WSGIServer(('127.0.0.1', 8000), app).serve_forever()�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�j   j  j�  �python�j�  �j�  }�uhj�  h h!hKbhjM  hhubeh}�(h]�h]�h]�h]�h]�uhjs  hj)  hhh h!hKpubeh}�(h]�h]�h]�h]�h]�j  �py�j  �method�j  j�  j  �uhj�  hhhju  h NhNubj�  )��}�(hhh]�h}�(h]�h]�h]�h]�h]��entries�]�(j�  �stop() (MiniHuey method)��MiniHuey.stop�hNt�auhj�  hju  hhh h!hNubj�  )��}�(hhh]�(j�  )��}�(h�stop()�h]�(j  )��}�(h�stop�h]�h.�stop�����}�(hhhj�  hhh NhNubah}�(h]�h]�h]�h]�h]�j   j  uhj  hj�  hhh h!hKsubj  )��}�(hhh]�h}�(h]�h]�h]�h]�h]�j   j  uhj  hj�  hhh h!hKsubeh}�(h]�j�  ah]�h]�j�  ah]�h]�jo  �jp  Njq  j�  jr  j�  uhj�  hj�  hhh h!hKsubjt  )��}�(hhh]�h:)��}�(h�Stop the scheduler.�h]�h.�Stop the scheduler.�����}�(hj�  hj�  hhh NhNubah}�(h]�h]�h]�h]�h]�uhh9h h!hKshj�  hhubah}�(h]�h]�h]�h]�h]�uhjs  hj�  hhh h!hKsubeh}�(h]�h]�h]�h]�h]�j  �py�j  �method�j  j�  j  �uhj�  hhhju  h h!hNubeh}�(h]�h]�h]�h]�h]�uhjs  hj�  hhh h!hKtubeh}�(h]�h]�h]�h]�h]�j  j�  j  �class�j  j�  j  �uhj�  hhhh$h NhNubh �note���)��}�(h��There is not a separate decorator for *periodic*, or *crontab*, tasks. Just
use :py:meth:`MiniHuey.task` and pass in a validation function. A
validation function can be generated using the :py:func:`crontab` function.�h]�h:)��}�(h��There is not a separate decorator for *periodic*, or *crontab*, tasks. Just
use :py:meth:`MiniHuey.task` and pass in a validation function. A
validation function can be generated using the :py:func:`crontab` function.�h]�(h.�&There is not a separate decorator for �����}�(h�&There is not a separate decorator for �hj  ubh�)��}�(h�
*periodic*�h]�h.�periodic�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhh�hj  ubh.�, or �����}�(h�, or �hj  ubh�)��}�(h�	*crontab*�h]�h.�crontab�����}�(hhhj  ubah}�(h]�h]�h]�h]�h]�uhh�hj  ubh.�, tasks. Just
use �����}�(h�, tasks. Just
use �hj  ubhA)��}�(h�:py:meth:`MiniHuey.task`�h]�hG)��}�(hj3  h]�h.�MiniHuey.task()�����}�(hhhj5  ubah}�(h]�h]�(hR�py��py-meth�eh]�h]�h]�uhhFhj1  ubah}�(h]�h]�h]�h]�h]��reftype��meth��	refdomain�j?  �refexplicit��hbNhcNhd�MiniHuey.task�hfhghh�uhh@h h!hKvhj  ubh.�U and pass in a validation function. A
validation function can be generated using the �����}�(h�U and pass in a validation function. A
validation function can be generated using the �hj  ubhA)��}�(h�:py:func:`crontab`�h]�hG)��}�(hjV  h]�h.�	crontab()�����}�(hhhjX  ubah}�(h]�h]�(hR�py��py-func�eh]�h]�h]�uhhFhjT  ubah}�(h]�h]�h]�h]�h]��reftype��func��	refdomain�jb  �refexplicit��hbNhcNhd�crontab�hfhghh�uhh@h h!hKvhj  ubh.�
 function.�����}�(h�
 function.�hj  ubeh}�(h]�h]�h]�h]�h]�uhh9h h!hKvhj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hh$hhh h!hNubj�  )��}�(h��Tasks enqueued for immediate execution will be run regardless of whether
the scheduler is running. You only need to start the scheduler if you plan
to schedule tasks in the future or run periodic tasks.�h]�h:)��}�(h��Tasks enqueued for immediate execution will be run regardless of whether
the scheduler is running. You only need to start the scheduler if you plan
to schedule tasks in the future or run periodic tasks.�h]�h.��Tasks enqueued for immediate execution will be run regardless of whether
the scheduler is running. You only need to start the scheduler if you plan
to schedule tasks in the future or run periodic tasks.�����}�(hj�  hj�  ubah}�(h]�h]�h]�h]�h]�uhh9h h!hK{hj�  ubah}�(h]�h]�h]�h]�h]�uhj�  hh$hhh h!hNubeh}�(h]�(�	mini-huey�heh]�h]�(�	mini-huey��mini�eh]�h]�uhh"hhhhh h!hK�expect_referenced_by_name�}�j�  hs�expect_referenced_by_id�}�hhsubeh}�(h]�h]�h]�h]�h]��source�h!uhh�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(h'N�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j�  �error_encoding��UTF-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h!�_destination�N�_config_files�]��pep_references�N�pep_base_url�� https://www.python.org/dev/peps/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��https://tools.ietf.org/html/��	tab_width�K�trim_footnote_reference_space���file_insertion_enabled���raw_enabled�K�syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_stylesheet���cloak_email_addresses���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}�h]�has�nameids�}�(j�  hj�  j�  j�  j�  j�  j�  j'  j'  j�  j�  u�	nametypes�}�(j�  �j�  Nj�  �j�  �j'  �j�  �uh}�(hh$j�  h$j�  j�  j�  j�  j'  j,  j�  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �id_start�K�parse_messages�]�(h �system_message���)��}�(hhh]�h:)��}�(hhh]�h.��duplicate object description of MiniHuey, other instance in /home/charles/code/huey/docs/contrib.rst, use :noindex: for one of them�����}�(hhhj(  ubah}�(h]�h]�h]�h]�h]�uhh9hj%  ubah}�(h]�h]�h]�h]�h]��level�K�type��WARNING��line�K$�source�h!uhj#  ubj$  )��}�(hhh]�h:)��}�(hhh]�h.��duplicate object description of MiniHuey.task, other instance in /home/charles/code/huey/docs/contrib.rst, use :noindex: for one of them�����}�(hhhjC  ubah}�(h]�h]�h]�h]�h]�uhh9hj@  ubah}�(h]�h]�h]�h]�h]��level�K�type�j=  �line�K*�source�h!uhj#  ubj$  )��}�(hhh]�h:)��}�(hhh]�h.��duplicate object description of MiniHuey.start, other instance in /home/charles/code/huey/docs/contrib.rst, use :noindex: for one of them�����}�(hhhj]  ubah}�(h]�h]�h]�h]�h]�uhh9hjZ  ubah}�(h]�h]�h]�h]�h]��level�K�type�j=  �line�KY�source�h!uhj#  ubj$  )��}�(hhh]�h:)��}�(hhh]�h.��duplicate object description of MiniHuey.stop, other instance in /home/charles/code/huey/docs/contrib.rst, use :noindex: for one of them�����}�(hhhjw  ubah}�(h]�h]�h]�h]�h]�uhh9hjt  ubah}�(h]�h]�h]�h]�h]��level�K�type�j=  �line�Kq�source�h!uhj#  ube�transform_messages�]�j$  )��}�(hhh]�h:)��}�(hhh]�h.�*Hyperlink target "mini" is not referenced.�����}�(hhhj�  ubah}�(h]�h]�h]�h]�h]�uhh9hj�  ubah}�(h]�h]�h]�h]�h]��level�K�type��INFO��source�h!�line�Kuhj#  uba�transformer�N�
decoration�Nhhub.