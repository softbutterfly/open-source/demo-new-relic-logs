import argparse
import glob
import gzip
import json
import multiprocessing
import os
import re
import subprocess
import urllib
import time
from datetime import datetime
from pytz import timezone

WHITE_SPACE_REGEX = re.compile(r" +")

ALLOWED_KIND = [
    "IN",
    "SEND",
]

ALLOWED_CHANNEL = [
    "apnsout",
    "google-fcm-out",
    "colhttp-out1",
    "colhttp-out2",
    "colhttp-out3",
    "colhttp-out4",
    "sparkpost-out",
]

COLLECTED_DATA = {}

def send_to_new_relic(filename):
    data_dir = os.path.dirname(os.path.realpath(__file__))
    file_path = os.path.join(data_dir, "json", filename)
    gzip_command = [
        "gzip",
        "-c",
        file_path
    ]
    gzip_process = subprocess.Popen(gzip_command, stdout=subprocess.PIPE)
    output, _ = gzip_process.communicate()

    sesion = urllib.URLopener()
    sesion.addheaders = sesion.addheaders + [
        (
            "X-Insert-Key",
            os.environ.get("NEW_RELIC_INSIGHTS_KEY", "my_new_relic_insights_key"),
        ),
        (
            "Content-Type",
            "application/json",
        ),
        (
            "Content-Encoding",
            "gzip",
        )
    ]
    response = sesion.open(
        "https://insights-collector.newrelic.com/v1/accounts/{NEW_RELIC_ACCOUNT_ID}/events".format(
            NEW_RELIC_ACCOUNT_ID=os.environ.get("NEW_RELIC_ACCOUNT_ID", "my_new_relic_account_id")
        ),
        data=output
    )
    response_content = response.read()

    print "New Relic response:", response_content

    if response.code == 200:
        os.remove(file_path)


class logtail(object):
    """
    This class can be used to tail a file and do something when new lines appear
    in it for logging.
    A typical usage case for this file might look like:
    python logtail.py --glob="auth.log" --directory="/var/log/" --single-file
    For a file that doesn"t rotate or:
    python logtail.py --glob="apache2_access.*" --directory="/var/log/apache2/"
    For something that does rotate
    """
    def __init__(self,globstr,directory,debug,singlefile,readsize=10000,recovery_file=None):
        self._glob = globstr
        self._directory = directory
        self._singlefile = singlefile
        self._debug = debug
        self._handle_gz = True
        self._freshstart = False
        self._recovery_file = recovery_file
        self._readsize = readsize
        self._last_timestamp = None

    @property
    def readsize(self):
        """
        This determines how many bytes are read from a file at a time.
        """
        return self._readsize

    @readsize.setter
    def readsize(self,value):
        self._readsize = value

    @property
    def globstr(self):
        """
        This contains the glob string that will be used for searching for files to parse.
        It will be assumed that there should be an ordered list of files resulting from
        the  directory/globstr combination unless the property "singlefile" is set.
        """
        return self._glob

    @globstr.setter
    def globstr(self, value):
        self._glob = value

    @property
    def directory(self):
        """
        The directory where we will search for matching files.
        """
        return self._directory

    @directory.setter
    def directory(self, value):
        self._directory = value

    @property
    def recovery_file(self):
        """
        The file that will remember the last file read for multifile read recovery.
        """
        return self._recovery_file

    @recovery_file.setter
    def recovery_file(self, value):
        self._recovery_file = value

    @property
    def singlefile(self):
        """
        If the globstr results in an absolute match, then this property should be set.
        This would be optimal for files like sys.log, where only one file rotates, but
        the current file only has one name.
        """
        return self._singlefile

    @singlefile.setter
    def singlefile(self, value):
        self._singlefile = value

    @property
    def debug(self):
        """Set for debug output."""
        return self._debug

    @debug.setter
    def debug(self,value):
        self._debug = value

    @property
    def handle_gz(self):
        """
        When apache2 rotates its files, it often gzips them.  This allows the collector
        to treat gzip files just like the regular files when recovering.
        """
        return self._handle_gz

    @handle_gz.setter
    def handle_gz(self, value):
        self._handle_gz = value

    @property
    def freshstart(self):
        """
        This tells the log collector to ignore any historic files or any knowledge
        needed for a restart, just go to the newest file and start tailing it from
        scratch (at the beginning of the file).
        """
        return self._freshstart

    @freshstart.setter
    def freshstart(self, value):
        self._freshstart = value

    def _fileglob(self):
        """
        Returns a list of files that matches the file glob information passed in.
        """
        files = glob.glob(os.path.join(self.directory,self.globstr))
        files.sort()
        return files

    def run(self):
        """
        Starts the log tailing and takes over the process in an infinite loop.
        """
        if ( self.debug ):
            print "Running."
        if ( self.singlefile ):
            self._run_singlefile()
        else:
            (filename,readcount) = ("",0)
            if ( self.recovery_file != None ):
                (filename,readcount) = self._recall()
            self._run_multifile(curfile=filename,totalread=readcount)

    def _run_singlefile(self):
        # For this run, we would need to check the file inode on each run, and when it
        # changes, reset the current read to zero.
        if ( self.debug ):
            print "Running singlefile."
        totalread = 0
        fileino = -1
        lastfileino = -1
        while ( 1 ):
            files = self._fileglob()
            if ( len(files) > 1 ):
                ValueError("Option singlefile set and more than one file returned by glob.");
            elif ( len(files) == 1 ):
                fileino = os.stat(files[0])[1];
                if ( fileino != lastfileino ):
                    lastfileino = fileino
                    totalread = 0
                totalread = self._run_file(filename=files[0],already_read=totalread);

    def _run_multifile(self,curfile="",totalread=0):
        recovery = False

        if ( self.debug ):
            print "Running multifile."
            if ( len(curfile) > 0):
                print "Running with recovery."
                recovery = True

        while ( 1 ):
            files = self._fileglob()
            skip_wait = False
            if ( len(files) ):
                if ( curfile == "" ):
                    curfile = files[-1]
                    totalread = 0
                elif ( curfile != files[-1] ):
                    idx = 0
                    for f in files:
                        if ( recovery and f == curfile or ( f[-3:] == ".gz" and f[:-3] == curfile ) ):
                            recovery = False
                            break
                        elif ( f > curfile ):
                            totalread = 0
                            break
                        else:
                            idx = idx + 1
                    curfile = files[idx]
                    if ( idx < len(files) - 1 ):
                        skip_wait = True
                totalread = self._run_file(filename=curfile,already_read=totalread,skip_wait=skip_wait)
            else:
                time.sleep(3)

    def _run_file(self,filename,already_read=0,skip_wait=False):
        fd = None
        is_gz = False
        if ( filename[-3:] == ".gz" ):
            fd = gzip.open(filename,"rb")
            is_gz = True
        else:
            fd = open(filename)
        if ( self.debug ):
            print fd
        totalread = already_read
        if ( totalread > 0 ):
            if ( is_gz ):
                fd.read(totalread)
            else:
                os.lseek(fd.fileno(),totalread,os.SEEK_SET)
        lastread = 0
        passes = 0
        while ( passes < 3 ):
            curread = ""
            if ( self.debug ):
                print "Waiting for pass: " + str(passes)
            lastread = totalread
            passes = passes + 1
            curread = ""
            if ( is_gz ):
                curread = fd.read(self.readsize)
            else:
                curread = os.read(fd.fileno(),self.readsize)
            remainder = ""
            while ( len(curread) > 0 ):
                passes = 0
                totalread = totalread + len(curread)
                self._remember(filename,totalread)
                lines = curread.split("\n")
                if ( len(remainder) ):
                    lines[0] = remainder + lines[0]
                    remainder = ""
                linecount = len(lines)
                for i in range(linecount):
                    line = lines[i]
                    if ( i < linecount - 1 ):
                        print filename
                        self.process(line)
                    elif ( len(line) > 0 ):
                        remainder = line
                if ( is_gz ):
                    curread = fd.read(self.readsize)
                else:
                    curread = os.read(fd.fileno(),self.readsize)
            if ( not skip_wait ):
                time.sleep(1)
        if ( self.debug ):
            print "Finishing file " + filename + " with read of: " + str(totalread)
        return totalread

    def _recall(self):
        filename = ""
        readamount = 0
        if ( self.recovery_file != None ):
            if ( os.path.isfile(self.recovery_file) ):
                fh = open(self.recovery_file,"r")
                for line in fh:
                    items = line.partition(":")
                    if ( len(items[2]) ):
                        filename = items[0]
                        readamount = int(items[2])
                        break
                    break
        return ( filename , readamount )

    def _remember(self,filename,readcount):
        if ( self.recovery_file != None ):
            if ( self.debug ):
                print "Filling " + self.recovery_file
            fh = open(self.recovery_file,"w")
            mod_filename = filename
            if ( mod_filename[-3:] == ".gz" ):
                mod_filename = mod_filename[:-3]
            fh.write("{0}:{1}\n".format(mod_filename,readcount))

    def process(self,line):
        """
        This is a subroutine that may be overridden in subclasses to change how this class deals
        with a line in the log file.
        """
        json_dir = "./json"
        json_filehandler = None
        json_data = {}

        if line.strip():
            line = line.strip()
            line = WHITE_SPACE_REGEX.sub(" ", line)
            line = line.split()
            timestamp = " ".join(line[0:2]).strip()

            if timestamp.startswith("[") and  timestamp.endswith("]"):
                timestamp = timestamp[1:-1].split(".")[0]

                print timestamp
                if self._last_timestamp != timestamp and self._last_timestamp is not None:
                    print "Current processing time", timestamp
                    print "Sending data from", self._last_timestamp
                    print "---"

                    task = multiprocessing.Process(
                        target=send_to_new_relic,
                        args=(
                            self._last_timestamp.replace(" ", "-").replace("/", "-"),
                        )
                    )
                    task.start()

                self._last_timestamp = timestamp
                json_filename = "{0}/{1}".format(json_dir, timestamp.replace(" ", "-").replace("/", "-"))

                if os.path.exists(json_filename):
                    json_filehandler = open(json_filename, "r")
                    json_data = json_filehandler.read().strip()
                    json_data = json.loads(json_data)[0] if json_data else {}

                timestamp_pattern = "%Y/%m/%d %H:%M:%S"

                if "." in timestamp:
                    timestamp_pattern = "%Y/%m/%d %H:%M:%S.%f"

                json_data["eventType"] = "LatiniaEvent"
                json_data["timestamp"] = datetime.strptime(timestamp, timestamp_pattern)
                json_data["timestamp"] = json_data["timestamp"].replace(tzinfo=timezone("UTC"))
                json_data["timestamp"] = time.mktime(json_data["timestamp"].timetuple())
                json_data["timestamp"] = int(json_data["timestamp"])
                json_data["date"] = timestamp.split()[0]
                json_data["time"] = timestamp.split()[1]

                try:
                    kind = line[2]
                    channel = line[3]

                    if kind in ALLOWED_KIND and channel in ALLOWED_CHANNEL:
                        key = "{0}--{1}".format(kind.upper(), channel.upper())

                        if key not in json_data:
                            json_data[key] = 1
                        else:
                            json_data[key] += 1

                        json_filehandler = open(json_filename, "w")
                        json_filehandler.write(json.dumps([json_data], indent=4))
                        json_filehandler.close()

                except:
                    pass


def default_arg_parser():
    """
    Creates an argparse ArgumentParser for parsing the command line when this utility is run as the
    main script.
    """
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument(
        "--glob","-g",
        type=str,
        required="True",
        dest="globstr",
        help="A file glob to help find files that need to be tailed.")
    arg_parser.add_argument(
        "--directory","-d",
        type=str,
        required="True",
        dest="directory",
        help="The directory to search for files.")
    arg_parser.add_argument(
        "--debug","-D",
        action="store_true",
        dest="debug",
        default=False,
        help="Turn on debugging output.")
    arg_parser.add_argument(
        "--single-file","-s",
        action="store_true",
        dest="singlefile",
        default=False,
        help="Use when running a single rotating file (sys.log, for instance).")
    arg_parser.add_argument(
        "--recovery-file","-r",
        type=str,
        dest="recovery_file",
        default=None,
        help="The file that will be used for recovery in multifile parsing.")
    return arg_parser


if __name__ == "__main__":
    arg_parser = default_arg_parser()
    options = arg_parser.parse_args()

    if options.debug == True :
        print options

    if os.path.isdir(options.directory) == True :
        lt = logtail(**(options.__dict__))
        lt.run()
