# DEMO NEW RELIC LOGS: Intercepcion de logs con python y publicacion en newrelic utilizando el API de New Relic Insights

Este demo muestra como emplear Python para interceptar logs de un aplicacion, extrar cierta informacion de los mismos y enviarlos a New Relic Insights con un tipo de evento personalizado.

El Proceso de muestra se compone de 2 script principales:
  1. `monitor/logger.py`:
     Emula la generaicon de un log de una aplicacion almacenandolo en .
  2. `monitor/collector.py`:
     Lee de forma incremental las nuevas lineas que aparecen el archivo de log generado por `monitor/logger.py` y los porcesa y envia a new relic.

## Como ejecutar este demo

1. Contruye la imagen de docker

```bash
docker build -f Dockerfile -t new-relic-logs:demo .
```

2. Copia el archivo `.env.template` con el nombre `.env` y coloca las variables de entorno indicadas.

```bash
cp .env.template .env
```

3. Ejecutar la imagen de docker que se ha creado

```bash
docker run --rm --name new-relic-demo --env-file $(pwd)/.env -it new-relic-logs:demo bash
```

Para poder ejecutar las tareas dentro de este contenedor vamosa ejecutar en 3 terminales adicionales el siguiente comando

```bash
docker exec -it new-relic-demo bash
```

4. Ejecutar los scripts

En la primera terminal vamos a ejecutar el monitor de procesos para ver como esta el consumo de recuros de nuestro contenedor.

```bash
top
```

En la segunda terminal vamos a preparar el proceso para validar la generacion de los logs

```bash
cd /monitor
watch -n 0.5 tail -10 logs/fake.log
```

En la tercera temrinal ejecutaremos el archivo `monitor/collector.py`

```bash
cd /monitor
python collector.py -g fake.log -d logs
```

Finalmente en la cuarta terminal ejecutaremos el archivo `monitor/logger.py`

```bash
cd /monitor
python logger.py
```

Luego de ejecutar todos esos comandos debemos tener una vista como esta de los procesos

![capture](resources/capture-01.png)

6. Despues de ejecutar los scripts veras en el panel de New Relic Insights que puedes consultar eventos del tipo `LatiniaEvent`

## Ejecucion como demonio en un entorno real

1. Para ejecutar este demo como demonio en sistemas unix/linux puedes modificar la funcion `process` en el archivo `monitor/collector.py` para que se ajuste a tus necesidades de procesamiento.

2. Debes de crear un archvio para tu gestor de procesos, sea `init`, `systemd` u otro. Ten encuenta que debes de configurar las variables de entorno que se muestran en el archivo `.env.template` para un correcto funcionamiento del script.

3. Como muestra se provee el archvio nr-custom-monitor.sh escrito para `init`.

4. No olvides instalar las dependencias de python adecuadas: `setuptools`, `argsparse` y `pytz`.


/etc/init.d/collecto-daemon
/logs/interception-and-instrumentation-on-centos6.10/monitor
